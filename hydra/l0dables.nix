{ rustManifest ? ./channel-rust-nightly.toml,
}:
let
  mozillaOverlay = import <mozillaOverlay>;
  manifestOverlay = self: super: {
    rustChannelOfTargets = _channel: _date: targets:
      (super.lib.rustLib.fromManifestFile rustManifest {
        inherit (super) stdenv fetchurl patchelf;
      }).rust.override { inherit targets; };
  };
  pkgs = (import <nixpkgs> { overlays = [ mozillaOverlay manifestOverlay ]; });
  project = import ../default.nix {
    inherit pkgs mozillaOverlay;
    firmwareSrc = <firmware>;
  };
  l0dables = project.l0dables.overrideAttrs (oldAttrs: {
    installPhase = ''
      ${oldAttrs.installPhase}

      mkdir -p $out/nix-support
      for f in $out/apps/*.elf ; do
        echo file binary-dist $f >> $out/nix-support/hydra-build-products
      done
    '';
  });
in {
  l0dables = pkgs.lib.hydraJob l0dables;
}
