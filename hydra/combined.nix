{ pkgs ? import <nixpkgs> {},
  cFirmware ? <c-firmware>,
  rustL0dables ? <rust-l0dables>,
}:
with pkgs;

let
  release = import ../release.nix {
    inherit cFirmware rustL0dables;
  };
  releaseZip = stdenv.mkDerivation {
    name = "card10-combined.zip";
    nativeBuildInputs = [ release zip ];
    phases = [ "installPhase" ];
    installPhase = ''
      mkdir -p $out/nix-support

      cd ${release}/
      zip -9r $out/firmware.zip .
      echo file binary-dist $out/firmware.zip >> $out/nix-support/hydra-build-products
    '';
  };
in {
  release = lib.hydraJob release;
  release-zip = lib.hydraJob releaseZip;
}
